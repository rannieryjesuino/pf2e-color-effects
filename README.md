Small content module which adds macros to place markers on tokens to keep track of, well, whatever you want.
Uses Pf2e's Effect items and Rule Elements.

**System compatability:**

The module requires the PF2E system and a Foundry version of V10 or higher.

**How to use:**

The effects do not need to be exported from compendiums to the items tab. 
Instead, You can use the provided macro while having a token selected to open the ChoiceSet dialogue, allowing you to select any of the provided effects while only using one macro slot. Remove the effect from the effects tab or the effects bar at the top right.

**Automation:**

The effects include no automation. They are purely visual indicators for whatever you want them to indicate.

**Preview:**

Macro opens easy dialogue to pick color (You can also hover over a token without selecting, and then press the macro hotkey!)

![Color Selection](https://gitlab.com/InfamousSky/pf2e-color-effects/-/raw/main/documentation/Apply%20Color.PNG)




All Colors included in the module

![All Colors](https://gitlab.com/InfamousSky/pf2e-color-effects/-/raw/main/documentation/Apply%20Color%202.PNG)




![Color Effect](https://gitlab.com/InfamousSky/pf2e-color-effects/-/raw/main/documentation/ColorEffect.webm)

**Version 1.1.0**
adds "Apply Number" Macro, which lets you add and remove a number from 1-9 on the token

![Counter](https://gitlab.com/InfamousSky/pf2e-color-effects/-/raw/main/documentation/Counter.webm)

**Version 1.2.0**
adds "ColorCounter" Macro, which combines the powers of colors and counters to let you place a number with a colored background

![colorCounter](https://gitlab.com/InfamousSky/pf2e-color-effects/-/raw/main/documentation/colorCounter.webm)

**Version 1.3.0**
adds "Action Tracker" Macro, which has icons for the Aid, Delay and Ready actions

![Action Tracker](https://gitlab.com/InfamousSky/pf2e-color-effects/-/raw/main/documentation/ActionTracker.webm)

**Version 1.4.0**
adds "Reaction used" Macro, which lets you track which actors have used their reaction. Do note that while it has a duration of one round, it does not get automatically removed at the start of the actor's turn, this requires the macro to be run again. (You can also give players observer permissions to this macro so they can add and remove this effect on their own tokens.)

**Version 1.5.0** Adds 2 more "Reaction Used" Macros, one with the number "2" to track various second reactions some Classes/Archetypes grant, and one with a Shield to track Quick Shield Block

![Reactions](https://gitlab.com/InfamousSky/pf2e-color-effects/-/raw/main/documentation/Reactions.webm)

**Version 1.6.0** Adds "ExpandedCounter" Macro, which lets you place a number from 0-99 on a token.

![Expanded Counter](https://gitlab.com/InfamousSky/pf2e-color-effects/-/raw/main/documentation/ExpandedCounter.webm)


**Version 2.0.0** Foundry v10 compatibility! Adds 3 new macros, the first one adding an Aura to a token with a custom range from 5 to 60 feet, and the second and third being variants of the Counter and Expanded Counter, making use of the new effect counter to incrementally update the count using the token effect panel!

![Custom Aura](https://gitlab.com/InfamousSky/pf2e-color-effects/-/raw/main/documentation/Aura.webm)

![Incremental Counters](https://gitlab.com/InfamousSky/pf2e-color-effects/-/raw/main/documentation/incrementalCounters.webm)

**Version 2.1.0** Adds Action Counter Macro, courtesy of Tikael (He/Him)#6851, Color Aura, and alternate versions of all counters and color markers which place the effects as hidden from players.

![Action Counter](https://gitlab.com/InfamousSky/pf2e-color-effects/-/raw/main/documentation/ActionCounter.webm)

![Color Auras](https://gitlab.com/InfamousSky/pf2e-color-effects/-/raw/main/documentation/ColorAura.PNG)

**Version 2.2.0** Adds a macro, which opens a journal containing links to all other macros in the module for easy access without clogging up the macro bar.